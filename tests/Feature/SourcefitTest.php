<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SourcefitTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLogs()
    {
        $response = $this->get('/api/logs');
        $response->assertStatus(200);
    }

    public function testUsers()
    {
        $response = $this->get('/api/users');
        $response->assertStatus(200);
    }

    public function testUserAdd()
    {   
        $payload = [
            "fname" => "walter",
            "mname" => "heisenberg",
            "lname" => "white",
            "contact_no" => "123123123123",
            "email" => "breaking@bad.com",
        ];

        $response = $this->postJson("/api/add-user", $payload);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            "fname",
            "mname",
            "lname",
            "contact_no",
            "email",
            "updated_at",
            "created_at",
            "id"
        ]);
    }

    public function testUserUpdate()
    {   
        $payload = [
            "fname" => "test",
            "mname" => "test",
            "lname" => "test",
            "contact_no" => "123123123123",
            "email" => "test@test.com",
        ];

        $response = $this->putJson("/api/update-user/1", $payload);
        $response->assertStatus(200);
    }

    public function testLogAdd()
    {   
        $payload = [
            "user_id" => 1,
            "date" => "2022-09-27",
            "time_in" => "9:23",
            "time_out" => "18:23"
        ];

        $response = $this->postJson("/api/add-log", $payload);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            "user_id",
            "date",
            "time_in",
            "time_out",
            "hours_worked",
            "hours_overtime",
            "hours_late",
            "hours_undertime",
            "updated_at",
            "created_at",
            "id"
        ]);
    }

    public function testUserDelete()
    {   
        $response = $this->deleteJson("/api/delete-user/1");
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "id",
            "fname",
            "mname",
            "lname",
            "contact_no",
            "email",
            "created_at",
            "updated_at"
        ]);
    }

}
