<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TimeTableController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('logs', [TimeTableController::class, 'getLogs']);
Route::post('add-log', [TimeTableController::class, 'addLog']);
Route::get('users', [TimeTableController::class, 'getUsers']);
Route::post('add-user', [TimeTableController::class, 'addUser']);
Route::put('update-user/{id}', [TimeTableController::class, 'updateUser']);
Route::delete('delete-user/{id}', [TimeTableController::class, 'deleteUser']);
