<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeTable extends Model
{

    protected $casts = [
        'time_in' => 'datetime:h:i A',
        'time_out' => 'datetime:h:i A',
    ];

    protected $fillable = [
        'user_id',
        'date',
        'time_in',
        'time_out',
        'hours_worked',
        'hours_overtime',
        'hours_late',
        'hours_undertime'
    ];

    use HasFactory;
}
