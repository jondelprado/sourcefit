<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRecord extends Model
{

    protected $fillable = ['fname', 'mname', 'lname', 'contact_no', 'email'];

    use HasFactory;
}
