<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TimeTable;
use App\Models\UserRecord;
use Carbon\Carbon;
use DB;

class TimeTableController extends Controller
{
    
    public function __construct(TimeTable $timetable, UserRecord $user)
    {
        $this->timetable = $timetable;
        $this->user = $user;
    }

    public function getLogs()
    {
        
        $get_logs = TimeTable::select('time_tables.*', DB::raw("CONCAT(user_records.lname,', ', user_records.fname) as name"))
        ->join('user_records', 'user_records.id', '=', 'time_tables.user_id')
        ->get();

        return $get_logs;
    }

    public function getUsers()
    {
        $get_users = UserRecord::all();
        return $get_users;
    }

    public function addLog(Request $request)
    {
        
        try {

            $request->validate([
                'date' => 'required',
                'time_in' => 'required',
                'time_out' => 'required',
            ]);

            $date = $request->all()['date'];
            $time_in = $request->all()['time_in'];
            $time_out = $request->all()['time_out'];
            $regular_hour = 8;
            $total_regular_hour = 9;
            $shift_start = "9:00";
            $hours_overtime = "00:00";

            $start = Carbon::createFromFormat('H:i', $shift_start);
            $in = Carbon::createFromFormat('H:i', $time_in);
            $out = Carbon::createFromFormat('H:i', $time_out);

            $hours_worked = $in->diff($out)->format('%H:%I');
            $split_worked = explode(":", $hours_worked);

            //GET OVERTIME
            if ((int)$split_worked[0] > 9) {
                $hours_overtime = (int)$split_worked[0] - $total_regular_hour;
            }

            //GET HOURS LATE
            if ($in->lt($start)) {
                $hours_late = "00:00";
            }
            else {
                $hours_late = $start->diff($in)->format('%H:%I');
            }

            //GET UNDERTIME
            $expected_hours = Carbon::createFromFormat('H:i', $in->addHours(9)->format('H:i'));
            $hours_undertime = $expected_hours->diff($out)->format('%H:%I');

            return $this->timetable->create(
                [
                    'user_id' => $request->all()['user_id'],
                    'date' => $date,
                    'time_in' => $time_in,
                    'time_out' => $time_out,
                    'hours_worked' => $hours_worked,
                    'hours_overtime' => $hours_overtime,
                    'hours_late' => $hours_late,
                    'hours_undertime' => $hours_undertime
                ]
            );

        } catch (\Exception $ex) {
            return response()->json(array(
                "status" => "Error",
                "message" => $ex->getMessage(),
            ));
        }

    }

    public function addUser(Request $request)
    {
        try {

            $request->validate([
                'fname' => 'required|max:100',
                'mname' => 'nullable|max:100',
                'lname' => 'required|max:100',
                'contact_no' => 'required|numeric',
                'email' => 'required',
            ]);

            return $this->user->create($request->all());

        } catch (\Exception $ex) {
            return response()->json(array(
                "status" => "Error",
                "message" => $ex->getMessage(),
            ));
        }
    }

    public function updateUser(Request $request, $id)
    {
        try {

            $request->validate([
                'fname' => 'required|max:100',
                'mname' => 'nullable|max:100',
                'lname' => 'required|max:100',
                'contact_no' => 'required|numeric',
                'email' => 'required',
            ]);

            return tap($this->user->findOrFail($id))->update($request->all());

        } catch (\Exception $ex) {
            return response()->json(array(
                "status" => "Error",
                "message" => $ex->getMessage(),
            ));
        }
    }

    public function deleteUser($id)
    {
        try {

            return tap($this->user->findOrFail($id))->delete();

        } catch (\Exception $ex) {
            return response()->json(array(
                "status" => "Error",
                "message" => $ex->getMessage(),
            ));
        }
    }
}
